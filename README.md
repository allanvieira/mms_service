<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="https://www.fourmilab.ch/hackdiet/e4/figures/figure627.png" alt="Logo" width="80" height="80">
  </a>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li> 
    <li><a href="#documentation">Documentation</a></li> 
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project


This is a microservice for managing simple moving means from crypto prices.



### Built With

* [GoLang](https://golang.org/)
* [Gin](https://github.com/gin-gonic/gin)
* [Gorm](https://gorm.io/index.html)
* [Validator](https://github.com/go-playground/validator)
* [Mockery](https://github.com/vektra/mockery)
* [Swag](https://github.com/vektra/mockery)
* [GoCov](https://github.com/axw/gocov)
* [Karate](https://github.com/intuit/karate)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* make
  ```sh
  sudo apt-get -y install make
  ```

* docker
  ```sh
  sudo apt-get update
  sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  sudo apt-get update
  sudo apt-get install docker-ce docker-ce-cli containerd.io
  ```

* docker-compose
  ```sh
  sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://allanvieira@bitbucket.org/allanvieira/mms_service.git
   ```

<!-- USAGE EXAMPLES -->
## Usage

The api use a MOCK API to read candles, to run reading data from production, change env `MERCADO_BITCOIN_URI` in `local.env` or `compose.env` files. 

1. Build Images
   ```sh
   make build
   ```

2. Run Local
   ```sh
   make compose
   ```
   After this, run file `src/main.go` in IDE

3. Run service as container
   ```sh
   make run
   ```

4. Run unit tests (result details : `/cover.out`) 
   ```sh
   make test-unit
   ```

5. Check coverage (result details : `/coverage.html`)
   ```sh
   make coverage
   ```

6. Run integration tests, (result details : `/karate-summary.html`)
   ```sh
   make test-integration
   ```

7. Get logs from container, (result details : `/moving_average_logs`)
   ```sh
   make get-logs
   ```
   
8. Install tools dependencies
   ```sh
   make tools
   ```

9. Update GoLang mocks, (result : `/src/core/_mocks`)
   ```sh
   make mocks
   ```

10. Update Swagger docs, (result : `/docs`)
   ```sh
   make swagger
   ```

11. Stop container service
   ```sh
   make stop
   ```

12. Up local compose to run service in an IDE
   ```sh
   make compose
   ```

<!-- DOCUMENTATION -->
## Documentation

With running service, check the swagger docs in `http://localhost:8180/v1/swagger/index.html`

Import our Postman Collection with request samples in `/docs/mms.postman_collection.json`

### Design

```
|--docs : Contains swagger and postman files
|--src
|   |__adapter : Third-party API integration layer
|   |__core : Bussiness Layer
|   |   |___interfaces : Ports contracts layer
|   |   |___mocks : Auto genrate mocks, dont edit manually
|   |   |__entity : Entity layer, contains bussiness structs
|   |   |__useCases : Core serives layer, implements services interfaces
|   |   |__valuesObjects : Contains keys and values  
|   |__infra : Infrastructure, implements interfaces of DB, Queues, Files, etc
|   |__presentation : REST API presentation layer  
|   |__repository : Data layer, contain the data models, implements repository interfaces
|__tests : Contains integrations tests and mocks APIs
```

### Flowcharts

![Alt text](docs/mms.png?raw=true "Title")

![Alt text](docs/flowchart.png?raw=true "Title")

###Get MMS

```sh 
    curl --location --request GET 'localhost:8180/v1/BRLETH/mms?from=1577836800&to=1606565306&rate=20'
```

###Get Executions

```sh 
    curl --location --request GET 'localhost:8180/v1/BRLETH/executions?from=1596565306'
```

###Known errors

1. Topics and subscriptions were not created:
```shell
  docker logs moving_average_mocks_1    
```

Return contains: 
```
NOT_FOUND: Subscription does not exist (resource=moving-average-notification-subscription)
```

Solution : Run make command again
Todo : Run services after topic and subscription are created 
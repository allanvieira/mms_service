FILES		?= $(shell find . -type f -name '*.go' -not -path "./vendor/*")
CURRENT_DIR = $(shell pwd)

default: help

help:   ## show this help
	@echo 'usage: make [target] ...'
	@echo ''
	@echo 'targets:'
	@egrep '^(.+)\:\ .*##\ (.+)' ${MAKEFILE_LIST} | sed 's/:.*##/#/' | column -t -c 2 -s '#'

tools:  ## fetch and install all required tools
	go get -u github.com/swaggo/swag/cmd/swag
	go get -u github.com/swaggo/gin-swagger
	go get -u github.com/swaggo/files
	go get github.com/vektra/mockery/v2/.../

clean:  ## go clean
	go clean

build: ## Build docker images
	docker build -t moving_average . --target run
	docker build -t karate-mock tests/. --target mock
	docker build -t karate tests/. --target integration_tests
	docker build -t moving_average_tests . --target test
	docker build -t moving_average_coverage . --target coverage

compose-down: ## down composer
	docker-compose down

compose-build: ## build composer
	docker-compose build postgres pubsub mocks

compose-up: ## up composer
	docker-compose up -d --force-recreate postgres pubsub
	sleep 10
	docker-compose up -d mocks

compose: compose-down compose-build compose-up
	echo Mocks are runing in http://localhost:9090

swagger: ## update Swagger Documents
	swag init -g src/main.go

mocks: ## update mocks application
	mockery --dir src/core/_interfaces/ --all --output src/core/_mocks

run: compose ## run local service
	sleep 20
	docker-compose up -d moving_average_service
	echo Service is running on localhost:8180
	echo Check API Swagger docs in http://localhost:8180/v1/swagger/index.html

stop: ## stop local service
	docker-compose down

get-logs: ## run local service
	docker cp moving_average_moving_average_service_1:/src/moving_average_logs $(CURRENT_DIR)

test-integration: run ## Run integration tests
	sleep 10
	docker rm moving_average_integration_1 || true
	docker run --network moving_average_default --env URL_MOVING_AVERAGE=http://moving_average_service:8180/v1 --env URL_MOCKS=http://mocks:9090 --name moving_average_integration_1 karate
	#docker cp moving_average_integration_1:/tests/target/karate-reports/karate-summary.html $(CURRENT_DIR)
	echo Check Details in karate-summary.html

test-unit: ## Run unit tests
	docker rm moving_average_tests_1 || true
	docker run --name moving_average_tests_1 moving_average_tests
	docker cp moving_average_tests_1:/out/cover.out $(CURRENT_DIR)
	echo Check Details in cover.out

coverage: ## Get coverage file
	docker rm moving_average_coverage_1 || true
	docker run --name moving_average_coverage_1 moving_average_coverage
	docker cp moving_average_coverage_1:/src/coverage.html $(CURRENT_DIR)
	echo Check Details in coverage.html

package queuePublisher

import (
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"moving_average/src/core/_interfaces"
)

type publisher struct {
	ctx   *context.Context
	topic *pubsub.Topic
}

func New(ctx *context.Context, client *pubsub.Client, topicID string) _interfaces.QueuePublisher {
	return &publisher{
		ctx:   ctx,
		topic: client.Topic(topicID),
	}
}

func (ref publisher) Publish(message interface{}) error {
	bytes, _ := json.Marshal(message)

	result := ref.topic.Publish(*ref.ctx, &pubsub.Message{Data: bytes})
	id, err := result.Get(*ref.ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	logrus.WithField("message_id", id).WithField("message_data", string(bytes)).
		Info("[Publisher] Published Execution Notification")
	return nil
}

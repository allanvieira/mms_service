package postgres

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

func OpenConnection(hostURI string, debug bool) *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags),
		logger.Config{
			SlowThreshold:             time.Second,
			IgnoreRecordNotFoundError: false,
			Colorful:                  false,
			LogLevel:                  logger.Error,
		},
	)

	if debug {
		newLogger.LogMode(logger.Info)
	}

	db, err := gorm.Open(postgres.Open(hostURI), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		logrus.Fatal(errors.WithStack(err))
	}

	return db
}

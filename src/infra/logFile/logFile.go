package logFile

import (
	"github.com/sirupsen/logrus"
	"io"
	"os"
)

const LogFile = "moving_average_logs"

func RegisterLogFile() func() {
	f, err := os.OpenFile("moving_average_logs", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		logrus.Fatalf("error on opening file : %v", err)
	}

	writer := io.MultiWriter(os.Stdout, f)
	logrus.SetOutput(writer)
	return func() {
		defer f.Close()
	}
}

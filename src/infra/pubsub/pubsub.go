package pubsub

import (
	"cloud.google.com/go/pubsub"
	"context"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/option"
)

func New(ctx context.Context, projectID string, options ...option.ClientOption) *pubsub.Client {
	client, err := pubsub.NewClient(ctx, projectID, options...)
	if err != nil {
		logrus.Fatalf("[PubSub] Failed on create new client : %+v", errors.WithStack(err))
	}

	return client
}

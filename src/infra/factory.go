package infra

import (
	"context"
	"gorm.io/gorm"
	"moving_average/src/core/_interfaces"
	"moving_average/src/infra/postgres"
	"moving_average/src/infra/pubsub"
	"moving_average/src/infra/queues/queuePublisher"
)

type factory struct {
	publisherNotification _interfaces.QueuePublisher
	db                    *gorm.DB
}

func New(ctx context.Context, pubsubProjectID, notificationTopicID, dbURI string, debug bool) _interfaces.FactoryInfra {
	pubsubClient := pubsub.New(ctx, pubsubProjectID)
	return &factory{
		publisherNotification: queuePublisher.New(&ctx, pubsubClient, notificationTopicID),
		db:                    postgres.OpenConnection(dbURI, debug),
	}
}

func (ref factory) GetPublisherNotification() _interfaces.QueuePublisher {
	return ref.publisherNotification
}

func (ref factory) GetDB() *gorm.DB {
	return ref.db
}

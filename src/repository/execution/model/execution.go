package model

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/lib/pq"
	"gorm.io/gorm"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type Execution struct {
	gorm.Model
	ID               uuid.UUID `gorm:"primary_key,unique"`
	Pair             valuesObjects.Pair
	From             time.Time
	To               time.Time
	Status           string
	Message          string
	CandlesTotal     int
	CandlesInserted  int
	CandlesUpdated   int
	AveragesTotal    int
	AveragesInserted int
	AverageUpdated   int
	AverageMissed    pq.StringArray `gorm:"type:text[]"`
	StartedAt        time.Time
	FinishedAt       time.Time
}

func (ref Execution) FromDomain(entity entity.Execution, uuidApplication uuid.UUID) *Execution {
	missedDays := make([]string, len(entity.AveragesMissed))
	for idx, m := range entity.AveragesMissed {
		missedDays[idx] = m.String()
	}

	model := &Execution{
		ID:               entity.ExecutionID,
		Pair:             entity.Pair,
		From:             entity.From,
		To:               entity.To,
		Status:           string(entity.Status),
		Message:          entity.Message,
		CandlesTotal:     entity.CandlesTotal,
		CandlesInserted:  entity.CandlesInserted,
		CandlesUpdated:   entity.CandlesUpdated,
		AveragesTotal:    entity.AveragesTotal,
		AveragesInserted: entity.AveragesInserted,
		AverageUpdated:   entity.AverageUpdated,
		StartedAt:        entity.StartedAt,
		FinishedAt:       entity.FinishedAt,
		AverageMissed:    missedDays,
	}

	if model.ID == uuid.Nil {
		model.ID = uuid.NewSHA1(uuidApplication, []byte(fmt.Sprintf("%s%s%s%s", model.Pair, model.From, model.To, model.StartedAt)))
	}

	return model
}

func (ref Execution) ToDomain() *entity.Execution {
	missedAverages := make([]*time.Time, len(ref.AverageMissed))
	for idx, m := range ref.AverageMissed {
		day, _ := time.Parse("2006-01-02 15:04:05 +0000 UTC", m)
		missedAverages[idx] = &day
	}

	return &entity.Execution{
		ExecutionID:      ref.ID,
		Pair:             ref.Pair,
		From:             ref.From,
		To:               ref.To,
		Status:           valuesObjects.Status(ref.Status),
		Message:          ref.Message,
		CandlesTotal:     ref.CandlesTotal,
		CandlesInserted:  ref.CandlesInserted,
		CandlesUpdated:   ref.CandlesUpdated,
		AveragesTotal:    ref.AveragesTotal,
		AveragesInserted: ref.AveragesInserted,
		AverageUpdated:   ref.AverageUpdated,
		AveragesMissed:   missedAverages,
		StartedAt:        ref.StartedAt,
		FinishedAt:       ref.FinishedAt,
	}
}

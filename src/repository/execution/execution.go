package execution

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"moving_average/src/repository/execution/model"
	"time"
)

type repository struct {
	db              *gorm.DB
	uuidApplication uuid.UUID
}

func (ref repository) Create(execution entity.Execution) (created *entity.Execution, err error) {
	record := model.Execution{}.FromDomain(execution, ref.uuidApplication)

	if db := ref.db.Create(&record); db.Error != nil {
		return nil, errors.WithStack(err)
	}

	return record.ToDomain(), nil
}

func (ref repository) Save(execution entity.Execution) (*entity.Execution, error) {
	record := model.Execution{}.FromDomain(execution, ref.uuidApplication)

	if db := ref.db.Save(&record); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	return ref.get(record.ID)
}

func (ref repository) SearchByPairDate(pair valuesObjects.Pair, from, to time.Time) ([]*entity.Execution, error) {
	var records []model.Execution
	if db := ref.db.Find(&records, "pair = ? and started_at >= ? and started_at <= ?", pair.ToString(), from, to); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	executions := make([]*entity.Execution, len(records))
	for idx, r := range records {
		executions[idx] = r.ToDomain()
	}

	return executions, nil
}

func (ref repository) get(executionID uuid.UUID) (*entity.Execution, error) {
	result := &model.Execution{}

	if db := ref.db.First(result, "id = ?", executionID); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	return result.ToDomain(), nil
}

func New(db *gorm.DB, uuidApplication uuid.UUID) _interfaces.ExecutionRepository {
	return &repository{
		db:              db,
		uuidApplication: uuidApplication,
	}
}

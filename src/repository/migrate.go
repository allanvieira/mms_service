package repository

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"moving_average/src/repository/candle/model"
	model2 "moving_average/src/repository/simpleAverage/model"
)

func Migrate(db *gorm.DB) error {
	if err := prevMigrations(db); err != nil {
		return errors.WithStack(err)
	}

	if err := fixedMigrations(db); err != nil {
		return errors.WithStack(err)
	}

	return temporaryMigrations(db)

}

func prevMigrations(db *gorm.DB) error {

	return nil
}

func fixedMigrations(db *gorm.DB) error {
	err := db.AutoMigrate(
		&model.Candle{},
		&model.CandleStage{},
		&model2.SimpleAverage{},
		&model2.SimpleAverageStage{},
	)
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func temporaryMigrations(db *gorm.DB) error {

	return nil
}

package simpleAverage

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"moving_average/src/repository/simpleAverage/model"
	"time"
)

type repository struct {
	db              *gorm.DB
	uuidApplication uuid.UUID
}

func (ref repository) Creates(averages []entity.SimpleAverage) ([]*entity.SimpleAverage, int, int, int, error) {
	if db := ref.db.Delete(&model.SimpleAverageStage{}, "true"); db.Error != nil {
		return nil, 0, 0, 0, errors.WithStack(db.Error)
	}

	recordsStage := make([]model.SimpleAverageStage, len(averages))
	for idx, a := range averages {
		recordsStage[idx] = *model.SimpleAverageStage{}.FromDomain(a, ref.uuidApplication)
	}

	if db := ref.db.Clauses(
		clause.OnConflict{UpdateAll: true}).Create(&recordsStage); db.Error != nil {
		return nil, 0, 0, 0, errors.WithStack(db.Error)
	}

	total, news, renews, err := ref.calcStatistics(averages[0].Pair)
	if err != nil {
		return nil, 0, 0, 0, errors.WithStack(err)
	}

	records := make([]model.SimpleAverage, len(recordsStage))
	for idx, r := range recordsStage {
		records[idx] = *model.SimpleAverage{}.FromStage(r)
	}

	if db := ref.db.Clauses(
		clause.OnConflict{UpdateAll: true}).Create(&records); db.Error != nil {
		return nil, 0, 0, 0, errors.WithStack(db.Error)
	}

	responses := make([]*entity.SimpleAverage, len(averages))
	for idx, r := range records {
		responses[idx] = r.ToDomain()
	}

	return responses, total, news, renews, nil
}

func (ref repository) calcStatistics(pair valuesObjects.Pair) (int, int, int, error) {
	statistics := struct{ Total, News, Renews int }{}
	query := `
	select count(s.id) as total,
		sum(case when c.id is null then 1 else 0 end) news,
		sum(case when c.hash != coalesce(s.hash, c.hash) then 1 else 0 end) renews
		from simple_average_stages s
		left join simple_averages c on c.id = s.id
		where s.pair = '%s'
	`
	if db := ref.db.Raw(fmt.Sprintf(query, pair.ToString())).Scan(&statistics); db.Error != nil {
		return 0, 0, 0, errors.WithStack(db.Error)
	}
	return statistics.Total, statistics.News, statistics.Renews, nil
}

func (ref repository) SearchNullDates(pair valuesObjects.Pair) ([]*time.Time, error) {
	var records []struct{ Day *time.Time }
	now := time.Now()
	trucateNow := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
	query := fmt.Sprintf(`
	with dates as (
		SELECT date_trunc('day', dd)::date as day
		FROM generate_series(current_date - 365, to_date('%s', 'YYYY-MM-DDTHH:MI:SS'), '1 day'::interval) dd
	)
	select d.day
	from dates as d	
			 left join simple_averages a on a.day = d.day and a.pair = '%s' 
	where a.id is null;
	`, trucateNow.String(), pair.ToString())

	if db := ref.db.Preload(clause.Associations).Raw(query).Scan(&records); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	days := []*time.Time{}
	for _, r := range records {
		days = append(days, r.Day)
	}

	return days, nil
}

func (ref repository) SearchByPairDate(pair valuesObjects.Pair, from, to time.Time) ([]*entity.SimpleAverage, error) {
	var records []model.SimpleAverage
	if db := ref.db.Find(&records, "pair = ? and day >= ? and day <= ?", pair.ToString(), from, to); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	averages := make([]*entity.SimpleAverage, len(records))
	for idx, r := range records {
		averages[idx] = r.ToDomain()
	}

	return averages, nil
}

func New(db *gorm.DB, uuidApplication uuid.UUID) _interfaces.SimpleAverageRepository {
	return &repository{db: db, uuidApplication: uuidApplication}
}

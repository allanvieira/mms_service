package model

import (
	"github.com/google/uuid"
	"moving_average/src/core/entity"
)

type SimpleAverageStage struct {
	SimpleAverage
}

func (ref SimpleAverageStage) FromDomain(average entity.SimpleAverage, uuidApplication uuid.UUID) *SimpleAverageStage {
	model := &SimpleAverageStage{
		SimpleAverage: *SimpleAverage{}.FromDomain(average, uuidApplication),
	}

	return model
}

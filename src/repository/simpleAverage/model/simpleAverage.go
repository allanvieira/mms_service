package model

import (
	"fmt"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"moving_average/src/repository/execution/model"
	"strconv"
	"time"
)

type SimpleAverage struct {
	gorm.Model
	ID          uuid.UUID       `gorm:"primary_key,unique"`
	ExecutionID uuid.UUID       `json:"-"`
	Execution   model.Execution `gorm:"foreignKey:ExecutionID;references:ID"`
	Pair        string
	Timestamp   int64
	Day         time.Time
	HasPrevious bool
	HasNext     bool
	MMS20       float64
	MMS50       float64
	MMS200      float64
	Hash        uuid.UUID
}

func (ref SimpleAverage) FromStage(average SimpleAverageStage) *SimpleAverage {
	return &SimpleAverage{
		ID:          average.ID,
		ExecutionID: average.ExecutionID,
		Pair:        average.Pair,
		Timestamp:   average.Timestamp,
		Day:         average.Day,
		HasPrevious: average.HasPrevious,
		HasNext:     average.HasNext,
		MMS20:       average.MMS20,
		MMS50:       average.MMS50,
		MMS200:      average.MMS200,
		Hash:        average.Hash,
	}
}

func (ref SimpleAverage) FromDomain(average entity.SimpleAverage, uuidApplication uuid.UUID) *SimpleAverage {
	model := &SimpleAverage{
		ID:          average.AverageID,
		ExecutionID: average.ExecutionID,
		Pair:        average.Pair.ToString(),
		Timestamp:   average.Timestamp,
		Day:         time.Unix(average.Timestamp, 0),
		HasPrevious: average.HasPrevious,
		HasNext:     average.HasNext,
		MMS20:       average.MMS20,
		MMS50:       average.MMS50,
		MMS200:      average.MMS200,
	}

	if model.ID == uuid.Nil {
		model.ID = uuid.NewSHA1(uuidApplication, []byte(fmt.Sprintf("%s%s", model.Pair, strconv.Itoa(int(model.Timestamp)))))
	}

	model.Hash = uuid.NewSHA1(uuidApplication, []byte(fmt.Sprintf("%s%s%s%s%s%s", model.ID, model.HasPrevious, model.HasNext, model.MMS20, model.MMS50, model.MMS200)))

	return model
}

func (ref SimpleAverage) ToDomain() *entity.SimpleAverage {
	return &entity.SimpleAverage{
		AverageID:   ref.ID,
		ExecutionID: ref.ExecutionID,
		Pair:        valuesObjects.Pair(ref.Pair),
		Timestamp:   ref.Timestamp,
		HasPrevious: ref.HasPrevious,
		HasNext:     ref.HasNext,
		MMS20:       ref.MMS20,
		MMS50:       ref.MMS50,
		MMS200:      ref.MMS200,
	}
}

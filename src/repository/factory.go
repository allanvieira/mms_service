package repository

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"moving_average/src/core/_interfaces"
	"moving_average/src/repository/candle"
	"moving_average/src/repository/execution"
	"moving_average/src/repository/simpleAverage"
)

type factory struct {
	candleRepository        _interfaces.CandleRepository
	simpleAverageRepository _interfaces.SimpleAverageRepository
	executionRepository     _interfaces.ExecutionRepository
	uuidApplication         uuid.UUID
}

func New(infraFactory _interfaces.FactoryInfra, uuidApplication uuid.UUID) _interfaces.FactoryRepository {
	db := infraFactory.GetDB()
	if err := Migrate(db); err != nil {
		logrus.Fatal(errors.WithStack(err))
	}

	return &factory{
		candleRepository:        candle.New(db, uuidApplication),
		simpleAverageRepository: simpleAverage.New(db, uuidApplication),
		executionRepository:     execution.New(db, uuidApplication),
		uuidApplication:         uuidApplication,
	}
}

func (ref factory) GetCandleRepository() _interfaces.CandleRepository {
	return ref.candleRepository
}

func (ref factory) GetSimpleAverageRepository() _interfaces.SimpleAverageRepository {
	return ref.simpleAverageRepository
}

func (ref factory) GetExecutionRepository() _interfaces.ExecutionRepository {
	return ref.executionRepository
}

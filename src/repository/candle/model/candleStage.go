package model

import (
	"github.com/google/uuid"
	"moving_average/src/core/entity"
)

type CandleStage struct {
	Candle
}

func (ref CandleStage) FromDomain(candle entity.Candle, uuidApplication uuid.UUID) *CandleStage {
	model := &CandleStage{*Candle{}.FromDomain(candle, uuidApplication)}
	return model
}

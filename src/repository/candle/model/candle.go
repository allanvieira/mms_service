package model

import (
	"fmt"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"moving_average/src/repository/execution/model"
	"strconv"
	"time"
)

type Candle struct {
	gorm.Model
	ID          uuid.UUID       `gorm:"primary_key,unique"`
	ExecutionID uuid.UUID       `json:"-"`
	Execution   model.Execution `gorm:"foreignKey:ExecutionID;references:ID"`
	Pair        string
	Timestamp   int64
	Day         time.Time
	Open        float64
	Close       float64
	High        float64
	Low         float64
	Volume      float64
	Hash        uuid.UUID
}

func (ref Candle) FromStage(candle CandleStage) *Candle {
	return &Candle{
		ID:          candle.ID,
		ExecutionID: candle.ExecutionID,
		Pair:        candle.Pair,
		Timestamp:   candle.Timestamp,
		Day:         candle.Day,
		Open:        candle.Open,
		Close:       candle.Close,
		High:        candle.High,
		Low:         candle.Low,
		Volume:      candle.Volume,
		Hash:        candle.Hash,
	}
}

func (ref Candle) FromDomain(candle entity.Candle, uuidApplication uuid.UUID) *Candle {
	model := &Candle{
		ID:          candle.CandleID,
		ExecutionID: candle.ExecutionID,
		Pair:        candle.Pair.ToString(),
		Timestamp:   candle.Timestamp,
		Day:         time.Unix(candle.Timestamp, 0),
		Open:        candle.Open,
		Close:       candle.Close,
		High:        candle.High,
		Low:         candle.Low,
		Volume:      candle.Volume,
	}

	if model.ID == uuid.Nil {
		model.ID = uuid.NewSHA1(uuidApplication, []byte(fmt.Sprintf("%s%s", model.Pair, strconv.Itoa(int(model.Timestamp)))))
	}

	model.Hash = uuid.NewSHA1(uuidApplication, []byte(fmt.Sprintf("%s%s", model.ID, model.Close)))

	return model
}

func (ref Candle) ToDomain() *entity.Candle {
	return &entity.Candle{
		CandleID:    ref.ID,
		ExecutionID: ref.ExecutionID,
		Pair:        valuesObjects.Pair(ref.Pair),
		Timestamp:   ref.Timestamp,
		Day:         ref.Day,
		Open:        ref.Open,
		Close:       ref.Close,
		High:        ref.High,
		Low:         ref.Low,
		Volume:      ref.Volume,
	}
}

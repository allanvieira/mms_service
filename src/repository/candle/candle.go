package candle

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"moving_average/src/repository/candle/model"
	"time"
)

type repository struct {
	db              *gorm.DB
	uuidApplication uuid.UUID
}

func (ref repository) Create(candle entity.Candle) (responseCandle *entity.Candle, err error) {
	record := model.Candle{}.FromDomain(candle, ref.uuidApplication)

	if db := ref.db.Create(&record); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	return record.ToDomain(), nil
}

func (ref repository) Creates(candles []entity.Candle) ([]*entity.Candle, int, int, int, error) {
	if db := ref.db.Delete(&model.CandleStage{}, "true"); db.Error != nil {
		return nil, 0, 0, 0, db.Error
	}

	recordsStage := make([]model.CandleStage, len(candles))
	for idx, c := range candles {
		recordsStage[idx] = *model.CandleStage{}.FromDomain(c, ref.uuidApplication)
	}

	if db := ref.db.Clauses(clause.
		OnConflict{UpdateAll: true}).Create(&recordsStage); db.Error != nil {
		return nil, 0, 0, 0, errors.WithStack(db.Error)
	}

	total, news, renews, err := ref.calcStatistics(candles[0].Pair)
	if err != nil {
		return nil, 0, 0, 0, err
	}

	records := make([]model.Candle, len(recordsStage))
	for idx, r := range recordsStage {
		records[idx] = *model.Candle{}.FromStage(r)
	}

	if db := ref.db.Clauses(clause.
		OnConflict{UpdateAll: true}).Create(&records); db.Error != nil {
		return nil, 0, 0, 0, errors.WithStack(db.Error)
	}

	responses := make([]*entity.Candle, len(candles))
	for idx, c := range records {
		responses[idx] = c.ToDomain()
	}

	return responses, total, news, renews, nil
}

func (ref repository) GetLastCandleDay(pair valuesObjects.Pair) (*time.Time, error) {
	lastDay := struct{ Day time.Time }{}
	query := `
	with dates as (
		SELECT date_trunc('day', dd)::date as day
		FROM generate_series(current_date - 565, current_date, '1 day'::interval) dd
	),
	 pairs as (
		 select '%s' as pair
	 ),
	 resume as (
		 select d.day,
				p.pair,
				c.id is not null as             has_candle,
				a.id is not null as             has_average,
				coalesce(a.has_next, false)     has_next,
				coalesce(a.has_previous, false) has_previous
		 from dates as d
				  join pairs as p on true
				  left join candles c on c.day = d.day and c.pair = p.pair
				  left join simple_averages a on a.day = d.day and a.pair = p.pair and a.day != current_date -1
	 )
	select min(day) as day
	from resume
	where not has_candle
	   or not has_average
	   or not has_next
	   or not has_previous;
	`

	if db := ref.db.Raw(fmt.Sprintf(query, pair.ToString())).Scan(&lastDay); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}
	return &lastDay.Day, nil
}

func (ref repository) GetCandlesByPair(pair valuesObjects.Pair) ([]*entity.Candle, error) {

	var candles []model.Candle

	if db := ref.db.Find(&candles, "pair = ?", pair.ToString()); db.Error != nil {
		return nil, errors.WithStack(db.Error)
	}

	response := make([]*entity.Candle, len(candles))

	for idx, candle := range candles {
		response[idx] = candle.ToDomain()
	}

	return response, nil
}

func (ref repository) calcStatistics(pair valuesObjects.Pair) (int, int, int, error) {
	statistics := struct{ Total, News, Renews int }{}
	query := `
	select count(s.id) as total,
		sum(case when c.id is null then 1 else 0 end) news,
		sum(case when s.hash != coalesce(c.hash, s.hash) then 1 else 0 end) renews
		from candle_stages s
		left join candles c on c.id = s.id
		where s.pair = '%s'
	`
	if db := ref.db.Raw(fmt.Sprintf(query, pair.ToString())).Scan(&statistics); db.Error != nil {
		return 0, 0, 0, errors.WithStack(db.Error)
	}
	return statistics.Total, statistics.News, statistics.Renews, nil
}

func New(db *gorm.DB, uuidApplication uuid.UUID) _interfaces.CandleRepository {
	return &repository{db: db, uuidApplication: uuidApplication}
}

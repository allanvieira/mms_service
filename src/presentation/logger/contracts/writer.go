package contracts

import (
	"bytes"
	"github.com/gin-gonic/gin"
)

type Writer struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

func (ref Writer) Write(b []byte) (int, error) {
	ref.Body.Write(b)
	return ref.ResponseWriter.Write(b)
}

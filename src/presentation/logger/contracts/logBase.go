package contracts

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/url"
	"strconv"
)

type LogBase struct {
	Fields logrus.Fields
}

func (ref LogBase) FromContext(c *gin.Context, timestamp string, latency int64, request, response interface{}) LogBase {
	path, _ := url.PathUnescape(c.Request.URL.Path)
	ref.Fields = logrus.Fields{}
	ref.Fields["request"] = request
	ref.Fields["contracts"] = response
	ref.Fields["response_status"] = strconv.Itoa(c.Writer.Status())
	ref.Fields["path"] = path
	ref.Fields["client_id"] = c.ClientIP()
	ref.Fields["timestamp"] = timestamp
	ref.Fields["method"] = c.Request.Method
	ref.Fields["params"] = c.Request.URL.Query()
	ref.Fields["headers"] = c.Request.Header
	ref.Fields["latency"] = latency
	ref.Fields["user_agent"] = c.Request.UserAgent()
	if c.Errors.Last() != nil {
		ref.Fields["error_message"] = c.Errors.Last().Err.Error()
	}
	return ref
}

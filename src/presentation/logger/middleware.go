package logger

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"moving_average/src/presentation/logger/contracts"
	"time"
)

func Middleware(c *gin.Context) {

	writer := contracts.Writer{Body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
	c.Writer = writer

	requestBuff, _ := ioutil.ReadAll(c.Request.Body)
	buffBody := ioutil.NopCloser(bytes.NewBuffer(requestBuff))
	buffCopy := ioutil.NopCloser(bytes.NewBuffer(requestBuff))
	c.Request.Body = buffCopy

	start := time.Now()
	c.Next()
	end := time.Now()

	var response interface{}
	var request interface{}

	json.Unmarshal(writer.Body.Bytes(), &response)

	buffRequest := new(bytes.Buffer)
	buffRequest.ReadFrom(buffBody)
	json.Unmarshal(buffRequest.Bytes(), &request)

	log := contracts.LogBase{}.FromContext(c, start.String(), end.Sub(start).Milliseconds(), request, response)
	if c.Writer.Status() >= 500 {
		logrus.WithFields(log.Fields).Error()
	} else if c.Writer.Status() >= 400 {
		logrus.WithFields(log.Fields).Warn()
	} else {
		logrus.WithFields(log.Fields).Info()
	}

}

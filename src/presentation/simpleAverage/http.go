package simpleAverage

import (
	"github.com/gin-gonic/gin"
	"moving_average/src/core/_interfaces"
	error2 "moving_average/src/presentation/error"
	"moving_average/src/presentation/simpleAverage/contracts"
	"net/http"
)

type api struct {
	averageService _interfaces.SimpleAverageService
}

func RegisterSimpleAverageAPI(group *gin.RouterGroup, averageService _interfaces.SimpleAverageService) {
	api := &api{averageService: averageService}

	group.GET(":pair/mms", api.Get)
}

// Get MMS godoc
// @Summary Get all mms given query params
// @Description Get all mms given query params
// @Tags mms
// @Accept json
// @Produce json
// @Param pair path string true "Pair ID" Enums(BRLBTC, BRLETH)
// @Param from query string true "Unix timestamp"
// @Param to query string true "Unix timestamp" default(yesterday)
// @Param range query string false "Range mms" Enums(20, 50, 200)
// @Success 200 {object} []contracts.AverageResponse
// @Failure 400 {object} error.ErrorsResponse
// @Failure 500 {object} error.Response
// @Router /{pair}/mms [get]
func (ref api) Get(c *gin.Context) {
	request := contracts.GetRequest{}

	if err := c.ShouldBindUri(&request.GetURI); err != nil {
		error2.HandleError(c, err)
		return
	}

	if err := c.ShouldBindQuery(&request.GetQuery); err != nil {
		error2.HandleError(c, err)
		return
	}

	averages, err := ref.averageService.SearchByPairDate(request.Pair, request.From, request.To)
	if err != nil {
		error2.HandleError(c, err)
		return
	}

	responses := make([]contracts.AverageResponse, len(averages))
	for idx, a := range averages {
		responses[idx] = responses[idx].FromDomain(*a, request.Range)
	}

	c.JSON(http.StatusOK, responses)
}

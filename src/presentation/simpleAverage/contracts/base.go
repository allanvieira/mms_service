package contracts

import "moving_average/src/core/entity"

type AverageBase struct {
	Timestamp int64   `json:"timestamp"`
	MMS       float64 `json:"mms"`
}

func (ref AverageBase) FromDomain(average entity.SimpleAverage, days int) AverageBase {
	response := AverageBase{Timestamp: average.Timestamp}
	switch days {
	case 20:
		response.MMS = average.MMS20
	case 50:
		response.MMS = average.MMS50
	case 200:
		response.MMS = average.MMS200
	}
	return response
}

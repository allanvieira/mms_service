package contracts

import "moving_average/src/core/entity"

type AverageResponse struct {
	AverageBase
}

func (ref AverageResponse) FromDomain(average entity.SimpleAverage, days int) AverageResponse {
	ref.AverageBase = ref.AverageBase.FromDomain(average, days)
	return ref
}

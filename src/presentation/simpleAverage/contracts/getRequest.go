package contracts

type GetRequest struct {
	GetURI
	GetQuery
}

type GetURI struct {
	Pair string `uri:"pair" binding:"required,oneof='BRLBTC' 'BRLETH'"`
}

type GetQuery struct {
	From  int64 `form:"from" binding:"required"`
	To    int64 `form:"to"`
	Range int   `form:"range" binding:"required,oneof=20 50 200"`
}

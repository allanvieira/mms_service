package error

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"moving_average/src/core/valuesObjects"
	"net/http"
	"reflect"
)

type Response struct {
	Error string `json:"error"`
}

type ErrorsResponse struct {
	Error []string `json:"error"`
}

func SendBadRequestError(c *gin.Context, err error) {
	errorsResponse := ErrorsResponse{
		Error: []string{},
	}
	errs, ok := err.(validator.ValidationErrors)
	if ok {
		errorsResponse = errorsValidatorsToResponse(errs)
	} else {
		errorsResponse.Error = append(errorsResponse.Error, errors.Cause(err).Error())
	}
	c.JSON(http.StatusBadRequest, errorsResponse)
}

func errorsValidatorsToResponse(validations validator.ValidationErrors) ErrorsResponse {
	errorsResponse := ErrorsResponse{
		Error: []string{},
	}
	for _, e := range validations {
		switch e.Tag() {
		case "required":
			errorsResponse.Error = append(errorsResponse.Error, fmt.Sprintf("%s is required", e.Field()))
		case "invalid":
			errorsResponse.Error = append(errorsResponse.Error, fmt.Sprintf("%s : %s is invalid", e.Field(), e.Param()))
		case "custom":
			errorsResponse.Error = append(errorsResponse.Error, e.Param())
		case "oneof":
			errorsResponse.Error = append(errorsResponse.Error, fmt.Sprintf("%s must contain someone from the list : %s", e.Field(), e.Param()))
		default:
			errorsResponse.Error = append(errorsResponse.Error, e.Param())
		}
	}
	return errorsResponse
}

func SendNotFoundError(c *gin.Context, err error) {
	errorsResponse := ErrorsResponse{
		Error: []string{errors.Cause(err).Error()},
	}
	c.JSON(http.StatusNotFound, errorsResponse)
}

func HandleError(c *gin.Context, err error) {
	log := logrus.WithField("exception", errors.Cause(err).Error()).
		WithField("type", reflect.TypeOf(errors.Cause(err))).
		WithField("detail", err)

	errorObject, ok := err.(*valuesObjects.Errors)
	if ok {
		log.Warn("Request Validation failed")
		handleErrors(c, errorObject)
		return
	}

	errorValidation, ok := err.(validator.ValidationErrors)
	if ok {
		log.Warn("Request Validation failed")
		errorsResponse := errorsValidatorsToResponse(errorValidation)
		c.JSON(http.StatusBadRequest, errorsResponse)
		return
	}

	log.Error("Request Internal Error")
	c.JSON(http.StatusInternalServerError, Response{Error: errors.Cause(err).Error()})

}

func handleErrors(c *gin.Context, err *valuesObjects.Errors) {

	switch err.Type {
	case valuesObjects.ErrorDuplicated:
		c.JSON(http.StatusConflict, err.Original)
		return
	case valuesObjects.ErrorNotFound:
		c.JSON(http.StatusNotFound, Response{Error: errors.New(err.Message).Error()})
		return
	case valuesObjects.ErrorValidation:
		err := ErrorsResponse{
			Error: []string{err.Error()},
		}
		c.JSON(http.StatusBadRequest, err)
		return
	default:
		logrus.Error(err)
		c.JSON(http.StatusInternalServerError, err)
		return
	}
}

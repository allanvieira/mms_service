package executions

import (
	"github.com/gin-gonic/gin"
	"moving_average/src/core/_interfaces"
	error2 "moving_average/src/presentation/error"
	"moving_average/src/presentation/executions/contracts"
	"net/http"
)

type api struct {
	executionService _interfaces.ExecutionService
}

func RegisterExecutionAPI(group *gin.RouterGroup, executionService _interfaces.ExecutionService) {
	api := &api{executionService: executionService}

	group.GET(":pair/executions", api.Get)
}

// Get Executions godoc
// @Summary Get all Executions given query params
// @Description Get all mms Executions query params
// @Tags Execution
// @Accept json
// @Produce json
// @Param pair path string true "Pair ID" Enums(BRLBTC, BRLETH)
// @Param from query string true "Unix timestamp"
// @Success 200 {object} []contracts.ExecutionBase
// @Failure 400 {object} error.ErrorsResponse
// @Failure 500 {object} error.Response
// @Router /{pair}/executions [get]
func (ref api) Get(c *gin.Context) {
	request := contracts.GetRequest{}

	if err := c.ShouldBindUri(&request.GetURI); err != nil {
		error2.HandleError(c, err)
		return
	}

	if err := c.ShouldBindQuery(&request.GetQuery); err != nil {
		error2.HandleError(c, err)
		return
	}

	executions, err := ref.executionService.SearchByPairDate(request.Pair, request.From, request.To)
	if err != nil {
		error2.HandleError(c, err)
		return
	}

	responses := make([]contracts.ExecutionBase, len(executions))
	for idx, a := range executions {
		responses[idx] = responses[idx].FromDomain(*a)
	}

	c.JSON(http.StatusOK, responses)
}

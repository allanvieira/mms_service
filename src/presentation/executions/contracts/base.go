package contracts

import (
	"moving_average/src/core/entity"
	"time"
)

type ExecutionBase struct {
	ExecutionID      string       `json:"execution_id"`
	Pair             string       `json:"pair"`
	From             time.Time    `json:"from"`
	To               time.Time    `json:"to"`
	Status           string       `json:"status"`
	Message          string       `json:"message"`
	CandlesTotal     int          `json:"candles_total"`
	CandlesInserted  int          `json:"candles_inserted"`
	CandlesUpdated   int          `json:"candles_updated"`
	AveragesTotal    int          `json:"averages_total"`
	AveragesInserted int          `json:"averages_inserted"`
	AverageUpdated   int          `json:"average_updated"`
	AveragesMissed   []*time.Time `json:"averages_missed"`
	StartedAt        time.Time    `json:"started_at"`
	FinishedAt       time.Time    `json:"finished_at"`
}

func (ref ExecutionBase) FromDomain(execution entity.Execution) ExecutionBase {
	return ExecutionBase{
		ExecutionID:      execution.ExecutionID.String(),
		Pair:             execution.Pair.ToString(),
		From:             execution.From,
		To:               execution.To,
		Status:           execution.Status.ToString(),
		Message:          execution.Message,
		CandlesTotal:     execution.CandlesTotal,
		CandlesInserted:  execution.CandlesInserted,
		CandlesUpdated:   execution.CandlesUpdated,
		AveragesTotal:    execution.AveragesTotal,
		AveragesInserted: execution.AveragesInserted,
		AverageUpdated:   execution.AverageUpdated,
		AveragesMissed:   execution.AveragesMissed,
		StartedAt:        execution.StartedAt,
		FinishedAt:       execution.FinishedAt,
	}
}

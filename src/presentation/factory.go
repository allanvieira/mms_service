package presentation

import (
	"fmt"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"moving_average/src/core/_interfaces"
	"moving_average/src/presentation/executions"
	"moving_average/src/presentation/logger"
	"moving_average/src/presentation/simpleAverage"
	"net/http"
)

const (
	relativePath = "v1"
	port         = "8180"
)

type factory struct {
	ginEngine      *gin.Engine
	factoryService _interfaces.FactoryService
}

func New(factoryService _interfaces.FactoryService) _interfaces.FactoryPresentation {
	ginEngine := gin.New()
	ginEngine.Use(gin.Recovery())
	return factory{
		ginEngine:      ginEngine,
		factoryService: factoryService,
	}
}

func (ref factory) Listen() {
	ref.registerAPI()

	ref.ginEngine.Run(fmt.Sprintf(":%s", port))
}

func (ref factory) registerAPI() {
	routerGroup := ref.ginEngine.Group(relativePath)
	routerGroup.Use(logger.Middleware)

	routerGroup.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	routerGroup.GET("/healthz", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})

	simpleAverage.RegisterSimpleAverageAPI(routerGroup, ref.factoryService.GetSimpleAverageService())
	executions.RegisterExecutionAPI(routerGroup, ref.factoryService.GetExecutionService())
}

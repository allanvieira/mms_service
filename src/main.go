package main

import (
	"context"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	_ "moving_average/docs"
	"moving_average/src/adapter"
	"moving_average/src/core/useCases"
	"moving_average/src/infra"
	"moving_average/src/infra/logFile"
	"moving_average/src/presentation"
	"moving_average/src/repository"
	"os"
	"strconv"
	"time"
)

var (
	dbURI               = os.Getenv("DATABASE_URI")
	mercadoBitcoinURI   = os.Getenv("MERCADO_BITCOIN_URI")
	pubsubProjectID     = os.Getenv("PUBSUB_PROJECT_ID")
	notificationTopicID = os.Getenv("PUBSUB_NOTIFICATION_TOPIC_ID")
	startCommand        = os.Getenv("START_COMMAND")
	debug               = os.Getenv("DEBUG")
	schedulerSeconds    = os.Getenv("SCHEDULER_SECONDS")
	seconds             = 86400
	uuidApplication     = uuid.MustParse("a4f273f2-e7c8-11eb-ba80-0242ac130004")
)

// @title Moving Average Service API
// @version 1.0
// @description This is a document swagger for Moving Average API

// @contact.name Allan Vieira
// @contact.url https://www.linkedin.com/in/allan-vieira-80473331/
// @contact.email vieira.allan@gmail.com

// @host localhost:8180
// @BasePath /v1

func main() {

	deferLof := logFile.RegisterLogFile()
	defer deferLof()

	ctx := context.Background()

	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.Info("Started Moving Average")

	debugMode := debug == "true"

	factoryInfra := infra.New(ctx, pubsubProjectID, notificationTopicID, dbURI, debugMode)
	factoryRepository := repository.New(factoryInfra, uuidApplication)
	factoryAdapter := adapter.New(mercadoBitcoinURI)
	factoryService := useCases.New(factoryRepository, factoryAdapter, factoryInfra)
	factoryPresentation := presentation.New(factoryService)

	factoryService.GetOrchestratorService().Scheduler(time.Duration(getSeconds()) * time.Second)

	factoryPresentation.Listen()

}

func getSeconds() int {
	parseSeconds, err := strconv.Atoi(schedulerSeconds)
	if err != nil {
		logrus.Warn("Cannot parse %+v in integer, using default value", schedulerSeconds)
		return seconds
	}
	return parseSeconds
}

package _interfaces

import "time"

type OrchestratorService interface {
	Exec() error
	Scheduler(duration time.Duration)
}

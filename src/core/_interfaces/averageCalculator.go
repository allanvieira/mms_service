package _interfaces

type AverageCalculator interface {
	Clean()
	Add(value float64)
	GetValue(days int) float64
}

package _interfaces

import (
	"github.com/google/uuid"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type SimpleAverageService interface {
	UpdateCandlesAverages(executionID uuid.UUID, pair valuesObjects.Pair) (total int, news int, renews int, err error)
	SearchByPairDate(pair string, from, to int64) ([]*entity.SimpleAverage, error)
	SearchNullDates(pair valuesObjects.Pair) ([]*time.Time, error)
}

type SimpleAverageRepository interface {
	SearchNullDates(pair valuesObjects.Pair) ([]*time.Time, error)
	SearchByPairDate(pair valuesObjects.Pair, from, to time.Time) ([]*entity.SimpleAverage, error)
	Creates(averages []entity.SimpleAverage) (responseAverages []*entity.SimpleAverage, total int, news int, renews int, err error)
}

package _interfaces

import "gorm.io/gorm"

type FactoryInfra interface {
	GetPublisherNotification() QueuePublisher
	GetDB() *gorm.DB
}

type FactoryRepository interface {
	GetCandleRepository() CandleRepository
	GetSimpleAverageRepository() SimpleAverageRepository
	GetExecutionRepository() ExecutionRepository
}

type FactoryAdapter interface {
	GetMercadoBitcoinCandleAdapter() CandleAdapter
}

type FactoryService interface {
	GetCandleService() CandleService
	GetSimpleAverageService() SimpleAverageService
	GetAverageCalculatorService() AverageCalculator
	GetExecutionService() ExecutionService
	GetOrchestratorService() OrchestratorService
}

type FactoryPresentation interface {
	Listen()
}

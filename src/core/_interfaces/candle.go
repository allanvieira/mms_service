package _interfaces

import (
	"github.com/google/uuid"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type CandleAdapter interface {
	ReadCandles(pair valuesObjects.Pair, from time.Time, to time.Time) ([]entity.Candle, error)
}

type CandleService interface {
	RefreshCandles(executionID uuid.UUID, pair valuesObjects.Pair, from time.Time) (total int, news int, renews int, err error)
	GetLastCandleDay(pair valuesObjects.Pair) (day *time.Time, err error)
}

type CandleRepository interface {
	Create(candle entity.Candle) (responseCandle *entity.Candle, err error)
	Creates(candles []entity.Candle) (responseCandles []*entity.Candle, total, news, renews int, err error)
	GetLastCandleDay(pair valuesObjects.Pair) (day *time.Time, err error)
	GetCandlesByPair(pair valuesObjects.Pair) (responseCandles []*entity.Candle, err error)
}

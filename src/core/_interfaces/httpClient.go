package _interfaces

type HttpClient interface {
	Get(path, params string) ([]byte, error)
}

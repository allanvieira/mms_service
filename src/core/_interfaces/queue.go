package _interfaces

type QueuePublisher interface {
	Publish(message interface{}) error
}

package _interfaces

import (
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type ExecutionService interface {
	New(pair valuesObjects.Pair, from, to time.Time) (*entity.Execution, error)
	Update(execution entity.Execution) (*entity.Execution, error)
	SearchByPairDate(pair string, from, to int64) ([]*entity.Execution, error)
}

type ExecutionRepository interface {
	Create(execution entity.Execution) (created *entity.Execution, err error)
	Save(execution entity.Execution) (*entity.Execution, error)
	SearchByPairDate(pair valuesObjects.Pair, from, to time.Time) ([]*entity.Execution, error)
}

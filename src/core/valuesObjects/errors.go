package valuesObjects

import "github.com/pkg/errors"

type Errors struct {
	error
	Type     ErrorsType
	Message  string
	Original interface{}
}

func NewErrors(errorsType ErrorsType, message string, original interface{}) error {
	if err := ValidateErrorsType(errorsType); err != nil {
		return err
	}
	return &Errors{
		error:    errors.New(message),
		Type:     errorsType,
		Message:  message,
		Original: original,
	}
}

func NewErrorsValidation(message string) error {
	return &Errors{
		error:   errors.New(message),
		Type:    ErrorValidation,
		Message: message,
	}
}

func NewErrorsDuplicated(original interface{}) error {
	return &Errors{
		Type:     ErrorDuplicated,
		Original: original,
	}
}

func NewErrorsNotFound(message string) error {
	return &Errors{
		error:   errors.New(message),
		Type:    ErrorNotFound,
		Message: message,
	}
}

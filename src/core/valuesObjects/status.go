package valuesObjects

type Status string

var (
	StatusSuccess = Status("SUCCESS")
	StatusIgnore  = Status("IGNORE")
	StatusWarning = Status("WARNING")
	StatusError   = Status("ERROR")
)

func (ref Status) ToString() string {
	return string(ref)
}

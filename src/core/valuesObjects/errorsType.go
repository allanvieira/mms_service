package valuesObjects

import "fmt"

type ErrorsType = string

var (
	ErrorDuplicated = "DUPLICATED"
	ErrorNotFound   = "NOT_FOUND"
	ErrorValidation = "VALIDATION"
)

var validErrorsType = map[string]ErrorsType{
	ErrorDuplicated: ErrorDuplicated,
	ErrorNotFound:   ErrorNotFound,
	ErrorValidation: ErrorValidation,
}

func ValidateErrorsType(errorsType ErrorsType) error {
	if _, in := validErrorsType[errorsType]; !in {
		return NewErrors(ErrorNotFound, fmt.Sprintf("Errors Type not found : %s", errorsType), nil)
	}
	return nil
}

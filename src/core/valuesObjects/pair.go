package valuesObjects

import (
	"fmt"
	"github.com/pkg/errors"
)

type Pair string

var (
	PairBRLBTC = "BRLBTC"
	PairBRLETH = "BRLETH"
)

var validPairs = map[string]Pair{
	PairBRLETH: Pair(PairBRLETH),
	PairBRLBTC: Pair(PairBRLBTC),
}

func (ref Pair) Validate() error {
	if _, in := validPairs[string(ref)]; !in {
		return errors.New(fmt.Sprintf("[Pair] Pair not found : %s", string(ref)))
	}
	return nil
}

func (ref Pair) ToString() string {
	return string(ref)
}

func AllValidPairs() []Pair {
	pairs := make([]Pair, len(validPairs))
	idx := 0
	for _, v := range validPairs {
		pairs[idx] = v
		idx++
	}
	return pairs
}

package candle

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type service struct {
	candleRepository _interfaces.CandleRepository
	candleAdapter    _interfaces.CandleAdapter
}

func New(candleRepository _interfaces.CandleRepository, candleAdapter _interfaces.CandleAdapter) _interfaces.CandleService {
	return &service{
		candleRepository: candleRepository,
		candleAdapter:    candleAdapter,
	}
}

func (ref service) RefreshCandles(executionID uuid.UUID, pair valuesObjects.Pair, from time.Time) (int, int, int, error) {
	candles, err := ref.candleAdapter.ReadCandles(pair, from, time.Now())
	if err != nil {
		return 0, 0, 0, errors.WithStack(err)
	}

	candles = ref.updateExecutionIDs(executionID, candles)

	_, total, news, renews, err := ref.candleRepository.Creates(candles)
	if err != nil {
		return 0, 0, 0, errors.WithStack(err)
	}

	return total, news, renews, nil
}

func (ref service) GetLastCandleDay(pair valuesObjects.Pair) (day *time.Time, err error) {
	return ref.candleRepository.GetLastCandleDay(pair)
}

func (ref service) updateExecutionIDs(executionID uuid.UUID, candles []entity.Candle) []entity.Candle {
	for idx, _ := range candles {
		candles[idx].ExecutionID = executionID
	}
	return candles
}

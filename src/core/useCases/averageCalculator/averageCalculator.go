package averageCalculator

import "moving_average/src/core/_interfaces"

type service struct {
	days   int
	count  int
	values []float64
}

func New() _interfaces.AverageCalculator {
	return &service{}
}

func (ref *service) Clean() {
	ref.values = []float64{}
	ref.count = 0
}

func (ref *service) Add(value float64) {
	ref.values = append(ref.values, value)
	ref.count++
}

func (ref *service) GetValue(days int) float64 {
	if len(ref.values) >= days {
		from := 0
		if ref.count > days-1 {
			from = ref.count - days
		}
		values := ref.values[from:]
		sum := 0.0
		for _, v := range values {
			sum += v
		}
		return sum / float64(days)
	}
	return 0
}

package execution

import (
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type service struct {
	executionRepository _interfaces.ExecutionRepository
}

func New(executionRepository _interfaces.ExecutionRepository) _interfaces.ExecutionService {
	return &service{executionRepository: executionRepository}
}

func (ref service) New(pair valuesObjects.Pair, from, to time.Time) (*entity.Execution, error) {
	execution := entity.Execution{
		Pair:      pair,
		From:      from,
		To:        to,
		StartedAt: time.Now(),
	}

	return ref.executionRepository.Create(execution)
}

func (ref service) Update(execution entity.Execution) (*entity.Execution, error) {
	return ref.executionRepository.Save(execution)
}

func (ref service) SearchByPairDate(pairName string, from, to int64) ([]*entity.Execution, error) {
	pair := valuesObjects.Pair(pairName)
	if err := pair.Validate(); err != nil {
		return nil, valuesObjects.NewErrorsValidation(err.Error())
	}

	fromDay := time.Unix(from, 0)
	if fromDay.Before(time.Now().Add(24 * -365 * time.Hour)) {
		return nil, valuesObjects.NewErrorsValidation("start date cannot be earlier than 365 days")
	}

	toDay := time.Unix(to, 0)
	if to == 0 {
		toDay = time.Now()
	}

	return ref.executionRepository.SearchByPairDate(pair, fromDay, toDay)
}

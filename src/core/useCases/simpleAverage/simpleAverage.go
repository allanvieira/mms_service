package simpleAverage

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"sort"
	"time"
)

type service struct {
	averageCalculator       _interfaces.AverageCalculator
	simpleAverageRepository _interfaces.SimpleAverageRepository
	candleRepository        _interfaces.CandleRepository
}

func New(averageCalculator _interfaces.AverageCalculator,
	simpleAverageRepository _interfaces.SimpleAverageRepository, candleRepository _interfaces.CandleRepository,
) _interfaces.SimpleAverageService {
	return &service{
		averageCalculator:       averageCalculator,
		simpleAverageRepository: simpleAverageRepository,
		candleRepository:        candleRepository,
	}
}

func (ref service) SearchNullDates(pair valuesObjects.Pair) ([]*time.Time, error) {
	return ref.simpleAverageRepository.SearchNullDates(pair)
}

func (ref service) SearchByPairDate(pairName string, from, to int64) ([]*entity.SimpleAverage, error) {
	pair := valuesObjects.Pair(pairName)
	if err := pair.Validate(); err != nil {
		return nil, valuesObjects.NewErrorsValidation(err.Error())
	}

	fromDay := time.Unix(from, 0)
	if fromDay.Before(time.Now().Add(24 * -365 * time.Hour)) {
		return nil, valuesObjects.NewErrorsValidation("start date cannot be earlier than 365 days")
	}

	toDay := time.Unix(to, 0)
	if to == 0 {
		toDay = time.Now()
	}

	return ref.simpleAverageRepository.SearchByPairDate(pair, fromDay, toDay)
}

func (ref service) UpdateCandlesAverages(executionID uuid.UUID, pair valuesObjects.Pair) (int, int, int, error) {
	logrus.Infof("[SimpleAverage] Get Candles ")
	candles, err := ref.getCandles(pair)
	if err != nil {
		return 0, 0, 0, err
	}

	logrus.Infof("[SimpleAverage] Calc Averages ")
	averages, err := ref.calcAverages(executionID, candles)
	if err != nil {
		return 0, 0, 0, err
	}

	if len(averages) > 0 {
		logrus.Infof("[SimpleAverage] updates Averages ")
		_, total, news, renews, err := ref.simpleAverageRepository.Creates(averages)
		return total, news, renews, err
	}

	return 0, 0, 0, nil
}

func (ref service) getCandles(pair valuesObjects.Pair) ([]*entity.Candle, error) {
	candles, err := ref.candleRepository.GetCandlesByPair(pair)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	sort.Slice(candles, func(i, j int) bool {
		return candles[i].Day.Before(candles[j].Day)
	})

	return candles, nil
}

func (ref service) calcAverages(executionID uuid.UUID, candles []*entity.Candle) ([]entity.SimpleAverage, error) {
	averages := make([]entity.SimpleAverage, len(candles))
	ref.averageCalculator.Clean()
	for idx, c := range candles {
		ref.averageCalculator.Add(c.Close)
		average := entity.SimpleAverage{
			ExecutionID: executionID,
			Pair:        c.Pair,
			Timestamp:   c.Timestamp,
			MMS20:       ref.averageCalculator.GetValue(20),
			MMS50:       ref.averageCalculator.GetValue(50),
			MMS200:      ref.averageCalculator.GetValue(200),
		}
		if idx > 0 {
			average.HasPrevious = c.Day.Add(time.Hour * -24).Equal(candles[idx-1].Day)
		}
		if idx < len(candles)-1 {
			average.HasNext = c.Day.Add(time.Hour * 24).Equal(candles[idx+1].Day)
		}
		averages[idx] = average
	}

	return averages, nil
}

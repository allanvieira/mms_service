package useCases

import (
	"github.com/go-playground/validator"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/useCases/averageCalculator"
	"moving_average/src/core/useCases/candle"
	"moving_average/src/core/useCases/execution"
	"moving_average/src/core/useCases/orchestrator"
	"moving_average/src/core/useCases/simpleAverage"
)

type factory struct {
	validator            *validator.Validate
	candleService        _interfaces.CandleService
	simpleAverageService _interfaces.SimpleAverageService
	averageCalculator    _interfaces.AverageCalculator
	executionService     _interfaces.ExecutionService
	orchestratorService  _interfaces.OrchestratorService
}

func New(factoryRepository _interfaces.FactoryRepository, factoryAdapter _interfaces.FactoryAdapter, factoryInfra _interfaces.FactoryInfra) _interfaces.FactoryService {
	newFactory := factory{}
	newFactory.validator = validator.New()
	newFactory.averageCalculator = averageCalculator.New()
	newFactory.simpleAverageService = simpleAverage.New(
		newFactory.averageCalculator,
		factoryRepository.GetSimpleAverageRepository(),
		factoryRepository.GetCandleRepository(),
	)
	newFactory.candleService = candle.New(
		factoryRepository.GetCandleRepository(),
		factoryAdapter.GetMercadoBitcoinCandleAdapter(),
	)
	newFactory.executionService = execution.New(factoryRepository.GetExecutionRepository())
	newFactory.orchestratorService = orchestrator.New(newFactory.candleService, newFactory.simpleAverageService, newFactory.executionService, factoryInfra.GetPublisherNotification())
	return newFactory
}

func (ref factory) GetCandleService() _interfaces.CandleService {
	return ref.candleService
}

func (ref factory) GetSimpleAverageService() _interfaces.SimpleAverageService {
	return ref.simpleAverageService
}

func (ref factory) GetAverageCalculatorService() _interfaces.AverageCalculator {
	return ref.averageCalculator
}

func (ref factory) GetExecutionService() _interfaces.ExecutionService {
	return ref.executionService
}

func (ref factory) GetOrchestratorService() _interfaces.OrchestratorService {
	return ref.orchestratorService
}

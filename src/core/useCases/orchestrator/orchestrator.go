package orchestrator

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"time"
)

type service struct {
	candleService        _interfaces.CandleService
	simpleAverageService _interfaces.SimpleAverageService
	executionService     _interfaces.ExecutionService
	publisher            _interfaces.QueuePublisher
}

func New(candleService _interfaces.CandleService, simpleAverageService _interfaces.SimpleAverageService,
	executionService _interfaces.ExecutionService, publisher _interfaces.QueuePublisher) _interfaces.OrchestratorService {
	return &service{
		candleService:        candleService,
		simpleAverageService: simpleAverageService,
		executionService:     executionService,
		publisher:            publisher,
	}
}

func (ref service) Exec() error {
	return ref.exec()
}

func (ref service) Scheduler(duration time.Duration) {
	logrus.Info("[Orchestrator] Scheduler Update")
	err := ref.exec()
	if err != nil {
		logrus.Error(err)
	}

	ticker := time.NewTicker(duration)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				logrus.Info("[Orchestrator] Scheduler Update")
				ref.exec()
			}
		}
	}()
}

func (ref service) exec() error {
	for _, pair := range valuesObjects.AllValidPairs() {
		execution, err := ref.processPair(pair)
		if err != nil {
			return errors.WithStack(err)
		}
		if execution.Status == valuesObjects.StatusWarning {
			_, err = ref.processPair(pair)
			if err != nil {
				return errors.WithStack(err)
			}
		}
	}
	return nil
}

func (ref service) processPair(pair valuesObjects.Pair) (*entity.Execution, error) {
	lastCandleDay, err := ref.candleService.GetLastCandleDay(pair)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	execution, err := ref.executionService.New(pair, *lastCandleDay, time.Now())
	if err != nil {
		return nil, errors.WithStack(err)
	}

	yesterday := time.Date(execution.To.Year(), execution.To.Month(), execution.To.Day(), 0, 0, 0, 0, time.UTC)
	yesterday = yesterday.Add(-24 * time.Hour)

	if execution.From.Equal(yesterday) {
		logrus.Infof("[Orchestrator] Ignoring execution : %+v", execution)
		execution.Status = valuesObjects.StatusIgnore
		return ref.finish(execution, nil)
	}

	execution.CandlesTotal, execution.CandlesInserted, execution.CandlesUpdated, err =
		ref.candleService.RefreshCandles(execution.ExecutionID, pair, *lastCandleDay)
	if err != nil {
		return ref.error(execution, err)
	}

	execution.AveragesTotal, execution.AveragesInserted, execution.AverageUpdated, err =
		ref.simpleAverageService.UpdateCandlesAverages(execution.ExecutionID, pair)
	if err != nil {
		return ref.error(execution, err)
	}

	missedAverages, err := ref.simpleAverageService.SearchNullDates(pair)
	if err != nil {
		return ref.error(execution, err)
	}
	if err != nil {
		return ref.error(execution, err)
	}

	if len(missedAverages) > 0 {
		execution.AveragesMissed = missedAverages
		execution.Status = valuesObjects.StatusWarning
	} else {
		execution.Status = valuesObjects.StatusSuccess
	}

	return ref.finish(execution, nil)
}

func (ref service) error(execution *entity.Execution, errMain error) (*entity.Execution, error) {
	execution.Status = valuesObjects.StatusError
	execution.Message = errMain.Error()
	return ref.finish(execution, errMain)
}

func (ref service) finish(execution *entity.Execution, errMain error) (*entity.Execution, error) {
	execution.FinishedAt = time.Now()
	saved, err := ref.executionService.Update(*execution)
	if err != nil {
		return execution, errors.WithStack(err)
	}

	err = ref.publisher.Publish(saved)
	if err != nil {
		return execution, errors.WithStack(err)
	}

	return execution, errMain
}

package entity

import (
	"github.com/google/uuid"
	"moving_average/src/core/valuesObjects"
	"time"
)

type Execution struct {
	ExecutionID      uuid.UUID
	Pair             valuesObjects.Pair
	From             time.Time
	To               time.Time
	Status           valuesObjects.Status
	Message          string
	CandlesTotal     int
	CandlesInserted  int
	CandlesUpdated   int
	AveragesTotal    int
	AveragesInserted int
	AverageUpdated   int
	AveragesMissed   []*time.Time
	StartedAt        time.Time
	FinishedAt       time.Time
}

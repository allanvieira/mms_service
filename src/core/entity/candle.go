package entity

import (
	"github.com/google/uuid"
	"moving_average/src/core/valuesObjects"
	"time"
)

type Candle struct {
	CandleID    uuid.UUID
	ExecutionID uuid.UUID
	Origin      string
	Pair        valuesObjects.Pair
	Timestamp   int64
	Day         time.Time
	Open        float64
	Close       float64
	High        float64
	Low         float64
	Volume      float64
}

package entity

import (
	"github.com/google/uuid"
	"moving_average/src/core/valuesObjects"
)

type SimpleAverage struct {
	AverageID   uuid.UUID
	ExecutionID uuid.UUID
	Pair        valuesObjects.Pair
	Timestamp   int64
	HasPrevious bool
	HasNext     bool
	MMS20       float64
	MMS50       float64
	MMS200      float64
}

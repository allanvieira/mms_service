package contracts

type CandleResponse struct {
	StatusCode      int          `json:"status_code"`
	StatusMessage   string       `json:"status_message"`
	ServerTimestamp int64        `json:"server_unix_timestamp"`
	Candles         []CandleBase `json:"candles"`
}

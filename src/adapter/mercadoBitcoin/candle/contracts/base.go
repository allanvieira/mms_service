package contracts

import (
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
)

type CandleBase struct {
	Timestamp int64   `json:"timestamp"`
	Open      float64 `json:"open"`
	Close     float64 `json:"close"`
	High      float64 `json:"high"`
	Low       float64 `json:"low"`
	Volume    float64 `json:"volume"`
}

func (ref CandleBase) ToDomain(pair valuesObjects.Pair) *entity.Candle {
	return &entity.Candle{
		Pair:      pair,
		Timestamp: ref.Timestamp,
		Open:      ref.Open,
		Close:     ref.Close,
		High:      ref.High,
		Low:       ref.Low,
		Volume:    ref.Volume,
	}
}

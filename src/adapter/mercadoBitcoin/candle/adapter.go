package candle

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"moving_average/src/adapter/mercadoBitcoin/candle/contracts"
	"moving_average/src/core/_interfaces"
	"moving_average/src/core/entity"
	"moving_average/src/core/valuesObjects"
	"strconv"
	"time"
)

const (
	GetCandle = "/v4/%s/candle"
)

type candleAdapter struct {
	client _interfaces.HttpClient
}

func NewCandleAdapter(client _interfaces.HttpClient) _interfaces.CandleAdapter {
	return &candleAdapter{client: client}
}

func (ref candleAdapter) ReadCandles(pair valuesObjects.Pair, from, to time.Time) ([]entity.Candle, error) {
	logrus.Infof("[MercadoBitcoinAdapter] Read Candles : %s From: %s To: %s", pair.ToString(), from.String(), to.String())
	path := fmt.Sprintf(GetCandle, pair)
	params := fmt.Sprintf("?from=%s&to=%s&precision=1d", strconv.Itoa(int(from.Unix())), strconv.Itoa(int(to.Unix())))

	response, err := ref.client.Get(path, params)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	logrus.Infof("[MercadoBitcoinAdapter] Response Candles : %s From: %s To: %s ", pair.ToString(), from.String(), to.String())

	candleResponse := contracts.CandleResponse{}
	err = json.Unmarshal(response, &candleResponse)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	candles := make([]entity.Candle, len(candleResponse.Candles))
	for idx, c := range candleResponse.Candles {
		candles[idx] = *c.ToDomain(pair)
	}

	return candles, nil
}

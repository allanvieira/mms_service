package adapter

import (
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	_interfaces2 "moving_average/src/core/_interfaces"
	"net/http"
)

type httpClient struct {
	client *http.Client
	host   string
}

func NewHttpClient(client *http.Client, host string) _interfaces2.HttpClient {
	return &httpClient{
		client: client,
		host:   host,
	}
}

func (ref httpClient) Get(path string, params string) (response []byte, err error) {
	uri := fmt.Sprintf("%s%s%s", ref.host, path, params)

	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	resp, err := ref.client.Do(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if resp.StatusCode == 404 {
		return nil, nil
	}

	response, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if resp.StatusCode != 200 {
		if len(response) > 0 {
			return nil, errors.New(string(response))
		}
		return nil, errors.New(fmt.Sprintf("Error making get http request, Status Code: %s, URI : %s", resp.Status, uri))
	}

	return
}

package adapter

import (
	"moving_average/src/adapter/mercadoBitcoin/candle"
	"moving_average/src/core/_interfaces"
	"net/http"
	"time"
)

type factory struct {
	MercadoBitcoinAdapterCandle _interfaces.CandleAdapter
}

func New(mercadoBitcoinURI string) _interfaces.FactoryAdapter {

	client := &http.Client{Timeout: 20 * time.Second}

	mercadoBitcoinClient := NewHttpClient(client, mercadoBitcoinURI)

	return &factory{
		MercadoBitcoinAdapterCandle: candle.NewCandleAdapter(mercadoBitcoinClient),
	}
}

func (ref factory) GetMercadoBitcoinCandleAdapter() _interfaces.CandleAdapter {
	return ref.MercadoBitcoinAdapterCandle
}

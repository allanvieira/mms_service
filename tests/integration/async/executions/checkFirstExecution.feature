Feature: Check First Execution 

Background:
    * def firsBRLETHExecution = call read('classpath:_data/firstExecutionBRLETH.js')
    * def firsBRLBTCExecution = call read('classpath:_data/firstExecutionBRLBTC.js')
    * def secondBRLBTCExecution = call read('classpath:_data/secondExecutionBRLBTC.js')
Scenario: Success First Executions

    Given url mockURL
    And path "/subscriber/notification"
    When method GET
    Then assert responseStatus == STATUS_OK
    And match response contains deep firsBRLETHExecution
    And match response contains firsBRLBTCExecution
    And match response contains secondBRLBTCExecution
    
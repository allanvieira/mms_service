function fn() {
    var URL_MOVING_AVERAGE = java.lang.System.getenv("URL_MOVING_AVERAGE");
    if (URL_MOVING_AVERAGE == undefined) URL_MOVING_AVERAGE = "http://localhost:8180/v1"
    var URL_MOCKS = java.lang.System.getenv("URL_MOCKS");
    if (URL_MOCKS == undefined) URL_MOCKS = "http://localhost:9090"

    return {
        mmsURL: URL_MOVING_AVERAGE,
        mockURL: URL_MOCKS,
        STATUS_OK: 200,
        STATUS_CREATED: 201,
        STATUS_BAD: 400,
        STATUS_NOT_FOUND: 404
    }
}
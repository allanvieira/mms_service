function GetSchema() {
    return {
        "ExecutionID": "#string",
        "Pair": "BRLETH",
        "From": "#string",
        "To": "#string",
        "Status": "SUCCESS",
        "Message": "",
        "CandlesTotal": 577,
        "CandlesInserted": 577,
        "CandlesUpdated": 0,
        "AveragesTotal": 577,
        "AveragesInserted": 577,
        "AverageUpdated": 0,
        "AveragesMissed": [],
        "StartedAt": "#string",
        "FinishedAt": "#string"
    }
}
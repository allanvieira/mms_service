function GetSchema() {
    return {
        "execution_id": "#string",
        "pair": "BRLBTC",
        "from": "#string",
        "to": "#string",
        "status": "WARNING",
        "message": "",
        "candles_total": 570,
        "candles_inserted": 0,
        "candles_updated": 0,
        "averages_total": 570,
        "averages_inserted": 0,
        "average_updated": 0,
        "averages_missed": "#[]",
        "started_at": "#string",
        "finished_at": "#string"
    }
}
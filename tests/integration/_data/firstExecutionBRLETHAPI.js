function GetSchema() {
    return {
        "execution_id": "#string",
        "pair": "BRLETH",
        "from": "#string",
        "to": "#string",
        "status": "SUCCESS",
        "message": "",
        "candles_total": 577,
        "candles_inserted": 577,
        "candles_updated": 0,
        "averages_total": 577,
        "averages_inserted": 577,
        "average_updated": 0,
        "averages_missed": [],
        "started_at": "#string",
        "finished_at": "#string"
    }
}
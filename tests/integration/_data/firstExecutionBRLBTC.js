function GetSchema() {
    return {
        "ExecutionID": "#string",
        "Pair": "BRLBTC",
        "From": "#string",
        "To": "#string",
        "Status": "WARNING",
        "Message": "",
        "CandlesTotal": 570,
        "CandlesInserted": 570,
        "CandlesUpdated": 0,
        "AveragesTotal": 570,
        "AveragesInserted": 570,
        "AverageUpdated": 0,
        "AveragesMissed": "#[]",
        "StartedAt": "#string",
        "FinishedAt": "#string"
    }
}
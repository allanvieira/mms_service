Feature: GET Execution API

Background:
    * def firsBRLETHExecution = call read('classpath:_data/firstExecutionBRLETHAPI.js')
    * def firsBRLBTCExecution = call read('classpath:_data/firstExecutionBRLBTCAPI.js')
    * def secondBRLBTCExecution = call read('classpath:_data/secondExecutionBRLBTCAPI.js')
Scenario: Success First BRLETH Executions

    Given url mmsURL
    And path "/BRLETH/executions"
    And params {"from": 1596565306 }
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains firsBRLETHExecution    

Scenario: Success First BRLBTC Executions

    Given url mmsURL
    And path "/BRLBTC/executions"
    And params {"from": 1596565306 }
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains firsBRLBTCExecution        
    And match response contains secondBRLBTCExecution        
    
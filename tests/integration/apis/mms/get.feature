Feature: GET MMS API

Background:
    * def firsBRLETHExecution = call read('classpath:_data/firstExecutionBRLETHAPI.js')
    * def firsBRLBTCExecution = call read('classpath:_data/firstExecutionBRLBTCAPI.js')
    * def secondBRLBTCExecution = call read('classpath:_data/secondExecutionBRLBTCAPI.js')
Scenario: Get Invalid Pair

    Given url mmsURL
    And path "/BRLET/mms"    
    When method GET    
    Then assert responseStatus == STATUS_BAD
    And match response == {"error":["Pair must contain someone from the list : 'BRLBTC' 'BRLETH'"]}

Scenario: Get Withous params

    Given url mmsURL
    And path "/BRLBTC/mms"    
    When method GET    
    Then assert responseStatus == STATUS_BAD
    And match response == {"error":["From is required","Range is required"]}

Scenario: Get Wrong range

    Given url mmsURL
    And path "/BRLBTC/mms"  
    And params {"from": 123, "range": 13}  
    When method GET    
    Then assert responseStatus == STATUS_BAD
    And match response == {"error":["Range must contain someone from the list : 20 50 200"]}

Scenario: Get Wrong from

    Given url mmsURL
    And path "/BRLBTC/mms"  
    And params {"from": 123, "range": 20}  
    When method GET    
    Then assert responseStatus == STATUS_BAD
    And match response == {"error":["start date cannot be earlier than 365 days"]}    
    
Scenario: Get Success BRLBTC 20 mms

    * def mms1 = {"timestamp":1626480000,"mms":173420.73549449997}
    * def mms2 = {"timestamp":1626566400,"mms":173108.84615049994}

    Given url mmsURL
    And path "/BRLBTC/mms"  
    And params {"from": 1626480000, "range": 20}  
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains mms1
    And match response contains mms2

Scenario: Get Success BRLBTC 50 mms

    * def mms1 = {"timestamp":1626480000,"mms":179532.19855360006}
    * def mms2 = {"timestamp":1626566400,"mms":179144.42967180003}

    Given url mmsURL
    And path "/BRLBTC/mms"  
    And params {"from": 1626480000, "range": 50}  
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains mms1
    And match response contains mms2    

Scenario: Get Success BRLBTC 200 mms

    * def mms1 = {"timestamp":1626480000,"mms":243823.38877994992}
    * def mms2 = {"timestamp":1626566400,"mms":243891.6041449999}

    Given url mmsURL
    And path "/BRLBTC/mms"  
    And params {"from": 1626480000, "range": 200}  
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains mms1
    And match response contains mms2        

Scenario: Get Success BRLETH 20mms 

    * def mms1 = {"timestamp":1626739200,"mms":10754.8294575}
    * def mms2 = {"timestamp":1626825600,"mms":10739.734954499998}

    Given url mmsURL
    And path "/BRLETH/mms"  
    And params {"from": 1626739200, "range": 20}  
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains mms1
    And match response contains mms2
        
Scenario: Get Success BRLETH 50mms 

    * def mms1 = {"timestamp":1626739200,"mms":11420.645070200002}
    * def mms2 = {"timestamp":1626825600,"mms":11355.229269000001}

    Given url mmsURL
    And path "/BRLETH/mms"  
    And params {"from": 1626739200, "range": 50}  
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains mms1
    And match response contains mms2    

Scenario: Get Success BRLETH 200mms 

    * def mms1 = {"timestamp":1626739200,"mms":11249.798538800002}
    * def mms2 = {"timestamp":1626825600,"mms":11281.454088599998}

    Given url mmsURL
    And path "/BRLETH/mms"  
    And params {"from": 1626739200, "range": 200}  
    When method GET    
    Then assert responseStatus == STATUS_OK
    And match response contains mms1
    And match response contains mms2    
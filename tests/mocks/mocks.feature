Feature: mock server

  Background:

    * def candlesBRLBTCResponse = read("classpath:_data/candles_BRLBTC.json")
    * def candlesBRLETHResponse = read("classpath:_data/candles_BRLETH.json")

    * def QueueSubscriber = Java.type('karateutils.QueueSubscriber')
    * def pubsubHost = java.lang.System.getenv('PUBSUB_EMULATOR_HOST') != undefined ? java.lang.System.getenv('PUBSUB_EMULATOR_HOST') : 'pubsub:8681';
    * def pubsubProjectID = java.lang.System.getenv('PUBSUB_PROJECT_ID') != undefined ? java.lang.System.getenv('PUBSUB_PROJECT_ID') : 'local';
    * def pubsubSubscriptionID = java.lang.System.getenv('PUBSUB_NOTIFICATION_SUBSCRIPTION_ID') != undefined ? java.lang.System.getenv('PUBSUB_NOTIFICATION_SUBSCRIPTION_ID') : 'moving-average-notification-subscription';
    * def subscriberNotification = new QueueSubscriber({'host': pubsubHost, 'project-id': pubsubProjectID, 'subscription-id': pubsubSubscriptionID})
    * def listenNotification = new java.lang.Thread(subscriberNotification.listen()).start();

  Scenario: pathMatches('/v4/BRLBTC/candle') && methodIs('get')
    * def responseStatus = 200
    * def response = candlesBRLBTCResponse

  Scenario: pathMatches('/v4/BRLETH/candle') && methodIs('get')
    * def responseStatus = 200
    * def response = candlesBRLETHResponse

  Scenario: pathMatches('/subscriber/notification') && methodIs('get')
    * string messageList = subscriberNotification.listMessages()
    * json jsonList = messageList
    * def fun = function (e) { return JSON.parse(e) }
    * def jsonList = karate.map(jsonList, fun)
    * def response = jsonList
    * def responseStatus = 200

  Scenario: pathMatches('/test') && methodIQueueSubscribers('get')
    * def responseStatus = 200
    * def response = "ok"
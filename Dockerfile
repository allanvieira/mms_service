FROM golang AS base
WORKDIR /src
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...

FROM base AS run
EXPOSE 8180
CMD ["src"]

FROM base AS test
RUN mkdir /out && go test -v -coverprofile=/out/cover.out ./...

FROM base AS coverage
RUN go get -u github.com/matm/gocov-html
RUN go get -u github.com/axw/gocov/gocov
RUN mkdir /out
RUN go test -cover -covermode=count -coverprofile=profile.cov ./...
RUN go tool cover -func profile.cov
RUN gocov convert profile.cov | gocov-html > coverage.html


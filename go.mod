module moving_average

go 1.16

require (
	cloud.google.com/go/pubsub v1.13.0 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/gin-gonic/gin v1.7.2
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/google/uuid v1.3.0
	github.com/jackc/pgtype v1.8.1 // indirect
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/tools v0.1.5 // indirect
	google.golang.org/api v0.51.0 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12
)
